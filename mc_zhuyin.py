#!/usr/bin/env python

from hanzi2reading.reading import Reading
from pathlib import Path
import hanzi2reading
import hanzi2reading.zhuyin as zhuyin
import json
import os
import sys

if "pack.mcmeta" not in os.listdir():
    print("Please run from inside of resource pack directory")
    print("Exiting")
    sys.exit(1)

home = str(Path.home())
index_file = f"{home}/.minecraft/assets/indexes"
resource_file = (
    f"{home}/.minecraft/resourcepacks/tw_zhuyin/assets/minecraft/lang/zh_tw.json"
)

dirs = os.listdir(index_file)
dirs.sort(reverse=True)
latest = dirs[0]

with open(f"{index_file}/{latest}") as index:
    versions = json.load(index)

tw_hash = versions["objects"]["minecraft/lang/zh_tw.json"]["hash"]
tw_head = tw_hash[:2]
lang_file = f"{home}/.minecraft/assets/objects/{tw_head}/{tw_hash}"

with open(lang_file) as f:
    json_file = json.load(f)

reading = Reading()
for k, v in json_file.items():
    result = reading.get(json_file[k])
    z = [zhuyin.get(s) for s in result]
    sentence = " ".join(z)
    if sentence:
        json_file[k] = f"{v} ({sentence})"

with open("assets/minecraft/lang/zh_tw.json", "w") as outfile:
    json.dump(json_file, outfile, indent=4)
